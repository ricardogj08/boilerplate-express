const express = require('express')
const app = express()
const bodyParser = require('body-parser')
require('dotenv').config()

/**
 * Montando Middlewares con Express.
 *
 * app.use(path, middlewareFunction)
 *
 * express.static(path) - Es un middleware que define una ruta para servir archivos estáticos.
 */
app.use('/public', express.static(__dirname + '/public'))

app.use((req, res, next) => {
  console.log(`${req.method} ${req.path} - ${req.ip}`)
  next()
})

/**
 * Monta el middleware para analizar formularios.
 */
app.use(bodyParser.urlencoded({ extended: false }))

/**
 * Definición de rutas con Express.
 *
 * app.METHOD(PATH, HANDLER)
 *
 * METHOD - Método HTTP.
 * @param PATH string: URI o ruta relativa en el servidor.
 * @param HANDLER function: Acción de respuesta.
 *   @param req object: Objecto de solicitud.
 *   @param res object: Objecto de respuesta.
 */

/*
app.get('/', (req, res) => {
  res.send('Hello Express')
})

console.log('Hello World')
*/

/**
 * __dirname - Ruta absoluta del fichero.
 */
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
})

app.get('/json', (req, res) => {
  let message = 'Hello json'

  if (process.env.MESSAGE_STYLE === 'uppercase') {
    message = message.toUpperCase()
  }

  res.json({ message })
})

/**
 * Se pueden encadenar Middlewares dentro de una ruta en especifico.
 */
app.get('/now', (req, res, next) => {
  req.time = new Date().toString()
  next()
}, (req, res) => {
  res.json({ time: req.time })
})

/**
 * Definición y obtención de parámetros de rutas con Express.
 */
app.get('/:word/echo', (req, res) => {
  res.json({ echo: req.params.word })
})

/**
 * Obtención de parámetros de consulta con Express.
 */
app.get('/name', (req, res) => {
  const first = req.query.first || ''
  const last = req.query.last || ''

  res.json({ name: `${first} ${last}` })
})

/**
 * Obtención del cuerpo de una petición con Express.
 */
app.post('/name', (req, res) => {
  const first = req.body.first || ''
  const last = req.body.last || ''

  res.json({ name: `${first} ${last}` })
})

module.exports = app
