# Basic Node and Express

This is the boilerplate code for the Basic Node and Express Challenges. Instructions for working on these challenges start at https://www.freecodecamp.org/learn/apis-and-microservices/basic-node-and-express/

[![Run on Repl.it](https://repl.it/badge/github/freeCodeCamp/boilerplate-express)](https://repl.it/github/freeCodeCamp/boilerplate-express)

## Notas

* `Node.js` - Es un entorno de ejecución de JavaScript que permite desarrollar aplicaciones backend.
* `Express` - Es un framework de desarrollo de aplicaciones web con Node.js
* `npm install` - Instala las dependencias del proyecto.
* `npm start` - Ejecuta el servidor Express.
* <http://localhost:3000>
* `__dirname` - Es una variable global de Node.js que retorna la ruta absoluta de un script de JavaScript.
* `.env` - Es un archivo shell que contiene todas las opciones de configuración de un proyecto.
* `process.env.VAR_NAME` - Es un objeto global de Node.js que permite acceder a las variables de entorno dentro de una aplicación.
* `dotenv` - Es un paquete que carga variables de entorno desde un archivo externo.
* `body-parser` - Es un paquete de Express que contiene middlewares para analizar el cuerpo de una petición.

## Referencias

* [Express](https://expressjs.com/)
* [dotenv](https://github.com/motdotla/dotenv)
* [body-parser](https://github.com/expressjs/body-parser)
